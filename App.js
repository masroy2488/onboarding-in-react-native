import React from 'react';
import { View, Text, SafeAreaView, StyleSheet, FlatList } from 'react-native';
import Onboarding from './src/components/Onboarding';

const App = () => {
  return (
    <>
      <SafeAreaView>
        <Onboarding />
      </SafeAreaView>
    </>
  )
}

export default App;