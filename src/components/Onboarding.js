import React, { useState, useRef } from 'react'
import { StyleSheet, Text, View, Animated, FlatList } from 'react-native'
import slides from '../slides'
import OnboardingItem from './OnboardingItem'
import Paginator from './Paginator'

const Onboarding = () => {
    const [currentIndex, setcurrentIndex] = useState(0 )
    const scrollX = useRef(new Animated.Value(0)).current;  
    const slidesRef = useRef(null)

    const viewabelItemsChanges = useRef(({ viewableItems }) => {
        setcurrentIndex(viewableItems[0].index)
    }).current;

    const useConfig = useRef({viewAreaCoveragePercentThreshold: 50}).current;

    return (
        <View style={styles.container}>
            <View>
                <FlatList
                    data={slides}
                    renderItem={({item}) => <OnboardingItem item={item} />}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled
                    bounces={false}
                    keyExtractor={(item) => item.id}
                    onScroll={Animated.event([{nativeEvent: {contentOffset: {x : scrollX}}}], {
                        useNativeDriver :false,  
                    })}
                    scrollEventThrottle={32}
                    onViewableItemsChanges={viewabelItemsChanges}
                    viewabilityConfig={useConfig}
                    res={slidesRef}
                />
            </View>
            <Paginator data={slides} scrollX={scrollX} />
        </View>
    )
}

export default Onboarding

const styles = StyleSheet.create({
    container:{
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
