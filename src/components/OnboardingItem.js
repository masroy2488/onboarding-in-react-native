import React from 'react'
import { StyleSheet, Text, View, Image, useWindowDimensions } from 'react-native'

const OnboardingItem = ({item}) => {

    const { width } = useWindowDimensions();

    return (
        <View style={[styles.container, {width}]}>
            <Image source={item.image} style={[ styles.image, {width, resizeMode:'contain'}]} />
            <View style={{flex: 0.3,}}>
                <Text style={styles.tittle}>{item.tittle}</Text>
                <Text style={styles.description}>{item.description}</Text>
            </View>
        </View>
    )
}

export default OnboardingItem

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
         flex: 0.7,
         justifyContent: 'center',
    },
    tittle: {
        fontWeight: '800',
        fontSize:28,
        marginBottom:10,
        color:'#493d81',
        textAlign:'center'
    },
    description : {
        fontWeight: '300',
        marginBottom:10,
        color:'#493d81',
        textAlign:'center',
        paddingHorizontal: 64,
    }
})
